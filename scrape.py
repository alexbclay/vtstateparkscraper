# coding: utf-8
import csv
import requests
from bs4 import BeautifulSoup
START_PAGE = 'https://securevtstateparks.com/cgi-bin/parkCGI.py'


def main():
    """
    """
    response = requests.get(START_PAGE)
    soup = BeautifulSoup(response.content, 'html.parser')
    form_data = get_form_data(soup)

    # park list page
    response = requests.post(START_PAGE, data=form_data)
    soup = BeautifulSoup(response.content, 'html.parser')
    form_data = get_form_data(soup)
    park_options = soup.find_all('option')
    park_list = []

    available_sites = []
    for park in park_options:
        park_list.append(park['value'])
        available_sites.extend(get_avail_sites(park['value'], dict(form_data)))
    from pprint import pprint as pp
    pp(available_sites)
    with open('parks.csv', 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=['park', 'name', 'type'])
        writer.writeheader()
        writer.writerows(available_sites)

def get_avail_sites(park, form_data):
    print('Getting sites for ' + park)
    # choose dates page
    form_data['park_name'] = park
    response = requests.post(START_PAGE, data=form_data)
    soup = BeautifulSoup(response.content, 'html.parser')
    form_data = get_form_data(soup)

    form_data['arrive_year'] = '2019'
    form_data['arrive_month'] = '6'
    form_data['arrive_day'] = '28'
    form_data['stay'] = 2
    form_data['bringpets'] = 'no'
    form_data['handicap'] = 'no'

    # select site page
    response = requests.post(START_PAGE, data=form_data)
    soup = BeautifulSoup(response.content, 'html.parser')

    even_rows = soup.find_all(attrs={'class': 'tableRowEven'})
    odd_rows = soup.find_all(attrs={'class': 'tableRowOdd'})
    sites = []
    for site in even_rows + odd_rows:
        # print(site)
        elems = site.find_all('td')
        print(elems[1].text.strip() + " - " + elems[2].text.strip())
        sites.append({'park': park,
                      'name': elems[1].text.strip(),
                      'type': elems[2].text.strip()})
    return sites


def get_form_data(soup):

    inputs = soup.find_all('input')
    form_data = {}
    for inp in inputs:
        form_data[inp['name']] = inp['value']
    # from pprint import pprint as pp
    # pp(form_data)
    return form_data


if __name__ == '__main__':
    main()
